import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('test-query')
  testQurey(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return {
      celsius: celsius,
      type: type,
    };
  }

  @Get('test-param/:celsius')
  testParam(@Req() req, @Param('celsius') celsius: number) {
    return {
      celsius: celsius,
    };
  }
  @Post('test-body')
  testBody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return {
      celsius: celsius,
    };
  }
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return {
      celsius: parseFloat(celsius),
      fahrenheit: (parseFloat(celsius) * 9.0) / 5 + 32,
    };
  }

  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return this.appService.covert(parseFloat(celsius));
  }

  @Post('convert')
  convertPost(@Body('celsius') celsius: number) {
    return {
      celsius: celsius,
      fahrenheit: (celsius * 9.0) / 5 + 32,
    };
  }
}
