import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemModule } from './tem/tem.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [TemModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
