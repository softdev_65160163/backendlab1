import { Module } from '@nestjs/common';
import { temController } from './tem.controller';
import { TemService } from './tem.service';

@Module({
  imports: [],
  exports: [],
  controllers: [temController],
  providers: [TemService],
})
export class TemModule {}
