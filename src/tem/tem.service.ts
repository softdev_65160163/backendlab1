import { Injectable } from '@nestjs/common';

@Injectable()
export class TemService {
  covert(celsius: number) {
    return {
      celsius: celsius,
      fahrenheit: (celsius * 9.0) / 5 + 32,
    };
  }
}
