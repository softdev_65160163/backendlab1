import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';

import { TemService } from './tem.service';
@Controller('tem')
export class temController {
  constructor(private readonly temService: TemService) {}
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return {
      celsius: parseFloat(celsius),
      fahrenheit: (parseFloat(celsius) * 9.0) / 5 + 32,
    };
  }

  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return this.temService.covert(parseFloat(celsius));
  }

  @Post('convert')
  convertPost(@Body('celsius') celsius: number) {
    return {
      celsius: celsius,
      fahrenheit: (celsius * 9.0) / 5 + 32,
    };
  }
}
